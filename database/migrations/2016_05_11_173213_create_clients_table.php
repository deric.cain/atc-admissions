<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id')->nullable();
            $table->string('student_name')->nullable();
            $table->string('payee_name');
            $table->string('payee_email')->nullable();
            $table->date('entry_date')->nullable();
            $table->string('address', 200);
            $table->string('city', 100);
            $table->string('state', 2);
            $table->string('zip', 10);
            $table->string('phone', 10);
            $table->string('program');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
