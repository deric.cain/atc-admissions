<?php

namespace App\Http\Controllers;

use App\Billing\Billing;
use App\Billing\StripeBilling;
use App\Helpers\Helpers;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Str;
use WePay;

class PaymentController extends Controller
{
    /**
     * PaymentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('templates.pay', [
            'states' => Helpers::states()
        ]);
    }

    /**
     * Post the payment, create transaction, etc.
     *
     * @param Request $request
     * @return bool
     */
    public function postPayment(Request $request)
    {
        $this->validate($request, [
            'student_name' => 'required',
            'payee_name' => 'required',
            'payee_email' => 'required|email',
            'entry_date' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        $transaction = new Billing($request->all());
        $transaction->execute();

        return json_encode(['success' => 'okay']);
    }

    /**
     * Cancels a subscription
     * @param $id
     */
    public function cancelSubscription($id)
    {
        $subscription = Subscription::find($id);
        $subscription->number_of_payments_left = 0;
        $subscription->next_payment = null;
        $subscription->save();
    }
}
