<?php

namespace App\Billing;

use App\Client;
use App\Subscription;
use Carbon\Carbon;
use Stripe\Charge;
use App\Customer;
use Stripe\Stripe;

/**
 * Billing.php
 * @author Deric Cain
 * @package Admissons Gateway
 */
class Billing implements BillingInterface
{
    /**
     * Get the form data from the request.
     *
     * @var array
     */
    private $request;

    /**
     * This is the Stripe customer creation response.
     *
     * @var array
     */
    private $customer;

    /**
     * @var Client
     */
    private $client;

    /**
     * Billing constructor. The request can be a request array or a Subscription object.
     *
     * @param Subscription|array $request
     */
    public function __construct($request)
    {
        if (env('APP_ENV') == 'production') {
            Stripe::setApiKey(env('STRIPE_LIVE_SECRET'));
        } else {
            Stripe::setApiKey(env('STRIPE_TEST_SECRET'));
        }
        $this->request = $request;
    }

    /**
     * Execute all of the required steps in order to process payment.
     */
    public function execute()
    {
        $this->establishCustomer();
        $this->chargeIntakeFees();
        $this->chargeTuitionFees();
        $this->chargeBusFees();
    }

    /**
     * See if the client already exists in out database and make that customer the client, if so.
     * If not, create the client in the database.
     *
     * @return bool
     */
    private function establishCustomer()
    {
        $client = Client::findByPhoneNumber($this->request['phone']);

        if ($client->count() > 0) {
            $this->client = $client;
            $this->customer = Customer::find($this->client->customer_id);
            $this->customer['source'] = $this->request['stripe_token'];
            try {
                $this->customer = $this->customer->save();
            } catch (\Stripe\Error\Card $e) {
                echo json_encode($e->getJsonBody());
                die;
            } catch (\Stripe\Error\InvalidRequest $e) {
                echo json_encode($e->getJsonBody());
                die;
            } catch (\Stripe\Error\Authentication $e) {
                echo json_encode($e->getJsonBody());
                die;
            } catch (\Stripe\Error\ApiConnection $e) {
                echo json_encode($e->getJsonBody());
                die;
            } catch (\Stripe\Error\Base $e) {
                echo json_encode($e->getJsonBody());
                die;
            }
        } else {
            $this->createCustomerInStripe();
            $this->createClientInDb();
        }
    }

    /**
     * Create a client in the database. This can be used for future
     * billing.
     */
    public function createClientInDb()
    {
        $this->client = Client::create([
            'student_name' => $this->request['student_name'],
            'payee_name' => $this->request['payee_name'],
            'payee_email' => $this->request['payee_email'],
            'entry_date' => Carbon::parse($this->request['entry_date']),
            'program' => $this->request['program'],
            'address' => $this->request['address'],
            'city' => $this->request['city'],
            'state' => $this->request['state'],
            'zip' => $this->request['zip'],
            'phone' => $this->request['phone'],
            'customer_id' => $this->customer['id'],
        ]);
    }

    /**
     * Charge the customer for services rendered.
     *
     * @param $type
     * @param $amount
     * @return mixed
     */
    public function charge($type, $amount)
    {
        try {
            Charge::create([
                'amount' => $amount,
                'currency' => 'usd',
                'description' => $type,
                'customer' => $this->customer['id'],
                'metadata' => [
                    'student_name' => $this->request['student_name'],
                    'payee_name' => $this->request['payee_name'],
                    'payee_email' => $this->request['payee_email'],
                    'entry_date' => Carbon::parse($this->request['entry_date']),
                    'program' => $this->request['program'],
                    'address' => $this->request['address'],
                    'city' => $this->request['city'],
                    'state' => $this->request['state'],
                    'zip' => $this->request['zip'],
                    'phone' => $this->request['phone']
                ],
                'receipt_email' => $this->request['payee_email']
            ]);
        } catch (\Stripe\Error\Card $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\InvalidRequest $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\Authentication $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\ApiConnection $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\Base $e) {
            echo json_encode($e->getJsonBody());
            die;
        }
    }

    /**
     * Create a subscription for recurring charges.
     *
     * @param $type
     * @param $numberOfPayments
     * @return mixed
     * @internal param bool $amountOfPayments
     */
    public function createSubscription($type, $numberOfPayments)
    {
        if ($type == 'intake') {
            $amount = $this->calculateInstallmentAmount();
            $nextPaymentOn = Carbon::today()->addMonth();
        } else {
            $amount = $this->convertFloatToInt($this->request['tuition_amount']);
            $nextPaymentOn = $this->getNextPaymentDate();
        }

        $this->client->subscriptions()->create([
            'next_payment' => $nextPaymentOn,
            'number_of_payments_left' => $numberOfPayments,
            'amount' => $amount,
            'type' => $type,
            'tuition_start_date' => Carbon::parse($this->request['tuition_start_date'])->toDateString(),
            'tuition_end_date' => Carbon::parse($this->request['tuition_end_date'])->toDateString(),
        ]);
    }

    /**
     * Charge one-time or recurring intake fees.
     */
    private function chargeIntakeFees()
    {
        if ($this->isPayingIntake()) {
            $this->charge('intake', $this->convertFloatToInt($this->request['intake_amount']));
        }

        if ($this->intakeIsInstallments()) {
            $this->createSubscription('intake', $this->request['installment_months']);
        }
    }


    /**
     * See if we need to charge an intake amount.
     *
     * @return boolean
     */
    private function isPayingIntake()
    {
        return $this->request['intake_amount'] > 0;
    }


    /**
     * See if there are tuition fees present.
     *
     * @return bool
     */
    private function isPayingTuition()
    {
        return $this->request['tuition_amount'] > 0;
    }

    /**
     * See if there is a bus fee present.
     *
     * @return bool
     */
    private function isPayingBusFees()
    {
        return $this->request['bus_fee_amount'] > 0;
    }

    /**
     * Check to see if the intake fees are split into installments.
     *
     * @return bool
     */
    private function intakeIsInstallments()
    {
        return array_has($this->request, 'installments');
    }

    /**
     * Check to see of the tuition is recurring.
     *
     * @return bool
     */
    private function tuitionIsRecurring()
    {
        return array_has($this->request, 'tuition_recurring');
    }

    /**
     * Charge the tuition fees. If they are recurring, set up
     * recurring payments. If it is a one-time payment, set
     * up a one-time payment. The create a transaction in
     * the local DB.
     */
    public function chargeTuitionFees()
    {
        if ($this->isPayingTuition()) {
            $this->createSubscription('tuition', $this->determineNumberOfPayments());
        }
    }

    /**
     * Charge bus fees.
     */
    private function chargeBusFees()
    {
        if ($this->isPayingBusFees()) {
            $this->charge('bus_fee', $this->convertFloatToInt($this->request['bus_fee_amount']),
                $this->request['stripe_token']);
        }
    }

    /**
     * Calculate the amount of each payment when paying installments.
     *
     * @return float
     */
    private function calculateInstallmentAmount()
    {
        return $this->convertFloatToInt(round($this->request['installment_intake_amount'] / $this->request['installment_months'],
            2));
    }

    /**
     * Creates a customer in Stripe. We can use this to charge them later if needed.
     */
    private function createCustomerInStripe()
    {
        try {
            $this->customer = Customer::create([
                'email' => $this->request['payee_email'],
                'source' => $this->request['stripe_token'],
                'metadata' => [
                    'student_name' => $this->request['student_name'],
                    'payee_name' => $this->request['payee_name'],
                    'payee_email' => $this->request['payee_email'],
                    'entry_date' => Carbon::parse($this->request['entry_date']),
                    'program' => $this->request['program'],
                    'address' => $this->request['address'],
                    'city' => $this->request['city'],
                    'state' => $this->request['state'],
                    'zip' => $this->request['zip'],
                    'phone' => $this->request['phone'],
                ]
            ]);
        } catch (\Stripe\Error\Card $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\InvalidRequest $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\Authentication $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\ApiConnection $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\Base $e) {
            echo json_encode($e->getJsonBody());
            die;
        }
    }

    /**
     * Stripe uses int instead of floats so we have to convert them.
     *
     * @param $number
     * @return mixed
     */
    private function convertFloatToInt($number)
    {
        return explode('.', $number)[0] . '00';
    }

    /**
     * See how many payments will be made.
     *
     * @return int
     */
    private function determineNumberOfPayments()
    {
        if($this->tuitionIsRecurring()) {
            $startDate = Carbon::parse($this->request['tuition_start_date']);
            $endDate = Carbon::parse($this->request['tuition_end_date']);

            return $startDate->diffInMonths($endDate);
        }

        return 1;
    }

    /**
     * We need to determine the date of the next payment. Usually, we use the entry
     * date, but if that date is in the past, then we need to use the current date.
     * Unless... the tuition is recurring, and then we need to use that start date.
     * @return string
     */
    private function getNextPaymentDate()
    {
        if($this->tuitionIsRecurring()) {
            return Carbon::parse($this->request['tuition_start_date'])->toDateString();
        }

        return ($this->isDateInThePast($this->request['entry_date'])) ? Carbon::today()->toDateString() : Carbon::parse($this->request['entry_date'])->toDateString();
    }

    /**
     * Determine if the date parameter is today.
     *
     * @param $date
     * @return bool
     */
    private function isDateInThePast($date)
    {
        return Carbon::today()->diffInDays(Carbon::parse($date), false) < 0;
    }
}
