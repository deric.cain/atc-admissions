<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Requests;

class CustomerController extends Controller
{

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Show all customers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('templates.customers', [
            'customers' => Customer::all()
        ]);
    }

    /**
     * Charge the customer for their payment.
     *
     * @param Request $request
     * @return string
     */
    public function chargeCustomer(Request $request)
    {
        Customer::charge($request->all());

        return json_encode(['success' => true]);
    }
}
