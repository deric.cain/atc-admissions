<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'student_name',
        'payee_name',
        'payee_email',
        'entry_date',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'customer_id',
        'program'
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'entry_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    /**
     * Find a client by their phone number.
     *
     * @param $query
     * @param $phoneNumber
     * @return mixed
     */
    public function scopeFindByPhoneNumber($query, $phoneNumber)
    {
        return $query->where('phone', $phoneNumber)->first();
    }

    /**
     * Get the client by their Stripe Customer ID.
     *
     * @param $query
     * @param $customerId
     * @return mixed
     */
    public function scopeFindByCustomerId($query, $customerId)
    {
        return $query->where('customer_id', $customerId)->first();
    }
}
