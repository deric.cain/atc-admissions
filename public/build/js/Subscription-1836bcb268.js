(function () {

    var $subscriptionsTable = $('#table-subscriptions');
    var $inactiveSubscriptionsTable = $('#table-inactive-subscriptions');
    var $cancelSubscription = $('.cancel-subscription');
    var BASE_URL = window.location;
    BASE_URL = BASE_URL.protocol + '//' + BASE_URL.hostname;

    var _ = {

        init: function () {
            this.loadTables();
            this.cancelSubscription();
        },

        loadTables: function () {
            $subscriptionsTable.DataTable({
                'pageLength': 50,
                'order': [[0, 'desc']],
            });

            $inactiveSubscriptionsTable.DataTable({
                'pageLength': 50,
                'order': [[0, 'desc']],
            });
        },

        cancelSubscription: function () {
            $cancelSubscription.on('click', function () {
                $target = $(this).data('id');
                console.log($target);
                swal({
                        title: "Are you sure?",
                        text: "This is not fixable...",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, stop subscription.",
                        cancelButtonText: "Nope! Exit.",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: 'GET',
                                url: BASE_URL + '/cancel/' + $target,
                                success: function(data) {
                                    console.log(data);
                                    window.location = BASE_URL + '/recurring';
                                },
                                error: function(error, xhr) {
                                    console.log(error + xhr)
                                }
                            })
                        } else {
                            swal('Close one...', 'The subscription has not been cancelled. Better watch those' +
                                ' fingers a little closer next time. You almost messed up.', 'info')
                        }
                    });
            });
        },

    };

    _.init();

})();
//# sourceMappingURL=Subscription.js.map
