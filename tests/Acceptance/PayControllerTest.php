<?php

namespace tests\ControllerTests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PayControllerTest extends \TestCase
{
    /** @test */
    function an_unauthenticated_user_is_redirected_to_login()
    {
        $this->visit('/')
            ->see('login');
    }

    /** @test */
    function an_authenticated_user_sees_the_payment_form()
    {
        $user = factory(\App\User::class)->create();

        $this->actingAs($user)
            ->visit('/')
            ->see('Credit Card Info');
    }

}