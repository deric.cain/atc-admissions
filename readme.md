# Admissions Gateway

*created by: Deric Cain* | deric.cain@gmail.com | [dericcain.com](https://dericcain.com)
## Purpose
The sole purpose of the Admissions Gateway is to process payments for intake and tuition fees for [Alabama Teen Challenge](https://alatc.org).
 
## Todo
- [ ] Create unit and acceptance tests
- [ ] ~~Create reports for Finance~~
    - [ ] ~~Transaction detail~~
    - [ ] ~~Program type~~
- [x] Add address fields to the payment form
- [x] Add ability to define the number of payments that will be made to tuition
- [x] Add ability to backdate tuition payments
 
## Method
The Admissions Gateway uses the [WePay API](http://wepay.com) to process payments. There are two types of payments.
 1. One-time Payments
 2. Recurring Payments
 
#### One-time Payments
One-time payments use Wepay's tokenization method to tokenize credit card information, via JS, and then submit payment to the WePay server using the `Billing.php` class.
 
#### Recurring Payments
Recurring payments are tokenized like the one-time payments, but then the `authorize` API call is made to authorize the card for future payments. A cron job runs everyday that looks for payments that need to be made. 
 
## Dependencies
* Laravel 5.2
* MySQL
* Bootstrap
* WePay PHP Client
* See composer.json for other dependencies

