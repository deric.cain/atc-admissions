<?php

namespace App;

use Stripe\Charge;
use Stripe\Stripe;

class Transaction
{

    /**
     * Return all of the charges within the Stripe dashboard.
     *
     * @return \Stripe\Collection
     */
    public static function all()
    {
        if (env('APP_ENV') == 'production') {
            Stripe::setApiKey(env('STRIPE_LIVE_SECRET'));
        } else {
            Stripe::setApiKey(env('STRIPE_TEST_SECRET'));
        }

        return Charge::all([
            'limit' => 500
        ])->data;
    }
}