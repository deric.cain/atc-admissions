<?php

namespace App\Http\Controllers;

use App\Client;
use App\Report;
use App\Subscription;
use App\Transaction;
use Illuminate\Http\Request;

use App\Http\Requests;
use Stripe\Charge;
use Stripe\Order;
use Stripe\Stripe;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show main reporting view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactions()
    {
        return view('templates.transactions', [
            'transactions' => Transaction::all(),
        ]);
    }

    /**
     * Show all of the active subscriptions.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subscriptions()
    {
        return view('templates.subscriptions', [
            'active' => Subscription::active(),
            'inactive' => Subscription::inactive(),
        ]);
    }

    public function reports()
    {
        return view('templates.reports', [
            'previousTotal' => Report::previousMonthTotal(),
            'currentTotal' => Report::currentMonthTotal(),
            'previousIntake' => Report::previousIntakes(),
            'currentIntake' => Report::currentIntakes(),
            'previousTuition' => Report::previousTuition(),
            'currentTuition' => Report::currentTuition(),
        ]);
    }

    public function chart()
    {
        return Report::buildChartData();
    }
}
