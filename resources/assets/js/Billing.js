(function () {

    var BASE_URL = window.location.protocol + '//' + window.location.hostname;
    var HOSTNAME = window.location.hostname;
    var $publishableKey = $('meta[name="stripe_key"]').attr('content');
    var $formPayment = $('#form-payment');
    var $btnSubmit = $('#btn-submit');
    var $paymentErrors = $('.payment-errors');
    var $errors = $paymentErrors.find('.errors');
    var $payeeName = $('#payee_name');
    var $payeeEmail = $('#payee_email');
    var $ccNumber = $('#cc_number');
    var $cvc = $('#cvc');
    var $ccMonth = $('#cc_month');
    var $ccYear = $('#cc_year');
    var $address = $('#address');
    var $city = $('#city');
    var $state = $('#state');
    var $zip = $('#zip');
    var $phone = $('#phone');
    var $spinner = $('.spinner');

    var _ = {


        /**
         * Show the progress spinner.
         */
        showSubmitProcessing: function () {
            $spinner.show();
            $btnSubmit.prop('disabled', true);
            $btnSubmit.addClass('btn-success').removeClass('btn-primary').html('<img class="spinner" src="images/spinner.svg">Processing...');
        },

        /**
         * Hide the progress spinner. If the submission is completed, then display completed.
         *
         * @param completed
         */
        hideSubmitProcessing: function (completed) {
            $spinner.hide();
            if (completed) {
                $btnSubmit.html('Completed');
            } else {
                $btnSubmit.prop('disabled', false);
                $btnSubmit.addClass('btn-primary').removeClass('btn-success').html('Submit');
            }
        },

        /**
         * Show the form submitted successfully.
         */
        showSuccess: function () {
            $spinner.hide();
            $btnSubmit.prop('disabled', false);
            $btnSubmit.html('The payment has posted!');
        },

        /**
         * Clear the form after submission.
         */
        clearForm: function () {
            $formPayment.find("input[type=text], textarea").val("");
        },

        /**
         * Display the errors in the submission.
         *
         * @param response
         */
        showCardError: function (response) {
            $errors.text(response);
            $paymentErrors.show();
            $formPayment.find('button').prop('disabled', false);
            _.hideSubmitProcessing();
        },

        /**
         * Handle the form submission using AJAX.
         */
        submitForm: function () {
            $formPayment.submit(function (event) {
                event.preventDefault();
                _.showSubmitProcessing();
                $paymentErrors.hide();
                if (!_.validatePhoneNumber($phone.val())) {
                    _.showCardError('Please enter a phone number that is 10 digits in length.');
                    return false;
                }

                Stripe.setPublishableKey($publishableKey);
                Stripe.card.createToken({
                    number: $ccNumber.val(),
                    cvc: $cvc.val(),
                    exp_month: _.makeMonth2Digits($ccMonth.val()),
                    exp_year: $ccYear.val(),
                    name: $payeeName.val(),
                    address_line1: $address.val(),
                    address_city: $city.val(),
                    address_state: $state.val(),
                    address_zip: $zip.val()
                }, stripeResponseHandler);

                function stripeResponseHandler(status, response) {
                    if (response.error) {
                        _.showCardError(response.error.message);
                    } else {
                        var token = response.id;
                        $formPayment.append($('<input type="hidden" name="stripe_token" />').val(token));
                        var url = this.action;
                        var data = $($formPayment).serialize();
                        $.ajax({
                            url: url,
                            data: data,
                            type: 'POST',
                            success: function (data) {
                                console.log(data);
                                var response = JSON.parse(data);
                                console.log(response);
                                if (response.error) {
                                    _.showCardError(response.error.message);
                                    _.hideSubmitProcessing();
                                } else {
                                    _.showSuccess();
                                    _.clearForm();
                                    setTimeout(_.hideSubmitProcessing, 5000);
                                }
                            },
                            error: function (response) {
                                console.log(response);
                                _.hideSubmitProcessing();
                            }
                        });
                    }
                }
            });
        },

        /**
         * Make credit card fields nice and pretty.
         */
        formatCreditCardFields: function() {
            $ccNumber.payment('formatCardNumber');
            $cvc.payment('formatCardCVC');
        },

        /**
         * Stripe only wants 2 digit months.
         *
         * @param month
         * @return {string}
         */
        makeMonth2Digits: function (month) {
            return (month.length < 2) ? '0' + month : month;
        },

        /**
         * Update the phone number field with the newly formatted number.
         */
        updatePhoneNumberField: function() {
            $phone.on('change', function() {
                $phone.val(_.formatPhoneNumber($phone.val()));
            })
        },

        /**
         * Formats a phone number to only use 10 digits.
         *
         * @param phoneNumber
         * @return string
         */
        formatPhoneNumber: function (phoneNumber) {
            if(phoneNumber.substr(0, 1) == '1') {
                phoneNumber = phoneNumber.substr(1, phoneNumber.length);
            }
            return phoneNumber.replace(/[^0-9]/g, '');
        },

        /**
         * Validate that we are using a real phone number.
         *
         * @param phoneNumber
         */
        validatePhoneNumber: function (phoneNumber) {
            return _.formatPhoneNumber(phoneNumber).length == 10;
        },

        /**
         * Use Bootstrap datepicker for date input fields.
         */
        initializeDatePickers: function() {
            $('.datepicker').datepicker({
                orientation: 'bottom'
            });
            $('.tuition-datepicker').datepicker({
                orientation: 'bottom',
                startDate: new Date()
            });
        },

        /**
         * Initialize events on load.
         */
        init: function () {
            this.updatePhoneNumberField();
            this.initializeDatePickers();
            this.formatCreditCardFields();
            $spinner.hide();
            $paymentErrors.hide();
        }
    };

    _.init();
    _.submitForm();

})();

