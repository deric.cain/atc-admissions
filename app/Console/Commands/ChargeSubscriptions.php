<?php

namespace App\Console\Commands;

use App\Billing\SubscriptionBilling;
use App\Subscription;
use Illuminate\Console\Command;

class ChargeSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Charges all of the subscriptions that need to be charged today.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = Subscription::chargeToday();
//        dd($subscriptions);
        $subscriptions->each(function($subscription) {
            $charge = new SubscriptionBilling($subscription);
            return $charge->charge();
        });
    }
}
