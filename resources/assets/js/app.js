$(document).ready(function(){

    var installments = $('input[name=installments]');
    var installmentsWrapper = $('.installments');
    var recurringTuition = $('input[name=tuition_recurring]');
    var tuitionDaterange = $('#tuition-daterange');

    installmentsWrapper.hide();
    tuitionDaterange.hide();

    installments.on('change', function() {
        if(installments.is(':checked')) {
            installmentsWrapper.show();
        } else {
            installmentsWrapper.hide();
        }
    });

    recurringTuition.on('change', function() {
        if(recurringTuition.is(':checked')) {
            tuitionDaterange.show();
        } else {
            tuitionDaterange.hide();
        }
    });

});
