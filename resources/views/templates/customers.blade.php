@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Customers</h2>
                <div class="table-responsive">
                    <table id="table-customers" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Customer Phone</th>
                            <th>Student Name</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers['data'] as $customer)
                            <tr>
                                <td>{{ $customer['metadata']['payee_name'] }}</td>
                                <td>{{ $customer['email'] }}</td>
                                <td class="customer-phone-number">{{ $customer['metadata']['phone'] }}</td>
                                <td>{{ $customer['metadata']['student_name'] }}</td>
                                <td class="text-right">
                                    <button class="btn btn-success btn-xs charge-customer"
                                            data-id="{{ $customer['id'] }}" data-toggle="modal"
                                            data-target="#payment-modal" data-customer-name="{{ $customer['metadata']['payee_name'] }}">Make
                                        Payment
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <!-- Modal -->
                    <div class="modal fade" id="payment-modal" tabindex="-1" role="dialog"
                         aria-labelledby="Payment Modal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="payment-modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <h4 class="payment-warning text-danger"></h4>
                                    {!! BootForm::open()->id('form-charge-customer') !!}
                                    {!! BootForm::text('Amount to charge', 'amount')->required() !!}
                                    {!! BootForm::select('Type of charge', 'type')->options([
                                        'intake' => 'Intake',
                                        'tuition' => 'Tuition',
                                        'bus_fare' => 'Bus Fare'
                                    ]) !!}
                                    <input type="hidden" name="customer_id">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" id="customer-charge-btn" class="btn btn-primary">Post
                                        Payment</button>
                                </div>
                                {!! BootForm::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.css"
          type="text/css">
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.js"></script>
    <script src="{{ elixir('js/Customer.js') }}"></script>
@endsection
