var elixir = require('laravel-elixir');
var livereload = require('laravel-elixir-livereload');

elixir(function(mix) {
    mix.sass('app.scss')
        .scripts([
            'app.js',
        ])
        .scripts([
            'Billing.js'
        ], './public/js/Billing.js')
        .scripts([
            'Report.js'
        ], './public/js/Report.js')
        .scripts([
            'Subscription.js'
        ], './public/js/Subscription.js')
        .scripts([
            'Customer.js'
        ], './public/js/Customer.js')
        .scripts([
            'Chart.js'
        ], './public/js/Chart.js')
        .version([
            './public/css/app.css',
            './public/js/all.js',
            './public/js/Billing.js',
            './public/js/Report.js',
            './public/js/Subscription.js',
            './public/js/Customer.js',
            './public/js/Chart.js',
        ])
        .livereload();
});
