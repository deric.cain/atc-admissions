@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Transaction List</h2>
                <div class="table-responsive">
                    <table id="table-transactions" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Program</th>
                            <th>Type</th>
                            <th>Student's Name</th>
                            <th>Contact's Name</th>
                            <th>Contact's Email Address</th>
                            <th>Entry Date</th>
                            <th>Status</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr class="{{ ($transaction->metadata->program === 'Hayden') ? 'border-danger' :
                            'border-info' }}">
                                <td><span class="time">{{ $transaction->created }}</span></td>
                                <td>{{ $transaction->metadata->program }}</td>
                                <td>{{ ucfirst(str_replace('_', ' ', $transaction->description)) }}</td>
                                <td>{{ $transaction->metadata->student_name }}</td>
                                <td>{{ $transaction->metadata->payee_name }}</td>
                                <td>{{ $transaction->metadata->payee_email }}</td>
                                <td><span class="entry-date">{{ $transaction->metadata->entry_date }}</span></td>
                                <td>
                                    <span class="label {{ ($transaction->status == 'succeeded') ? 'label-success' :
                                'label-danger'
                                }}">{{
                                ucfirst
                                ($transaction->status) }}</span>
                                </td>
                                <td>${{ number_format(($transaction->amount / 100), 2) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" type="text/css">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.4/moment-timezone.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="{{ elixir('js/Report.js') }}"></script>
@endsection
