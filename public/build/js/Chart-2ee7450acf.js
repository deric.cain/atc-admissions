(function(){

    var $chart = $('.chart');
    var chartData = [];
    var BASE_URL = window.location.protocol + '//' + window.location.hostname;
    var HOSTNAME = window.location.hostname;

    var _ = {
        init() {
            Highcharts.setOptions({
                lang: {
                    thousandsSep: ','
                }
            });

            this.getChartData();
        },

        getChartData() {
            $.getJSON('/chart').then(data => {
                chartData = data;
                _.buildChart();
            });
        },


        buildChart() {
            $chart.highcharts({
                chart: {
                    type: 'line',
                },
                title: {
                    text: 'Reports'
                },
                xAxis: {
                    categories: chartData.categories.reverse()
                },
                yAxis: {
                    title: {
                        text: 'Funding Types'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: [{
                    name: 'Total',
                    data: chartData.type.total.reverse(),
                    tooltip: {
                        valuePrefix: '$',
                    }
                }, {
                    name: 'Intake',
                    data: chartData.type.intake.reverse(),
                    tooltip: {
                        valuePrefix: '$',
                    }
                }, {
                    name: 'Tuition',
                    data: chartData.type.tuition.reverse(),
                    tooltip: {
                        valuePrefix: '$',
                    }
                }]
            });
        }
    }

    _.init();

})();
//# sourceMappingURL=Chart.js.map
