<?php

namespace App;

use Carbon\Carbon;

/**
 * Class Report
 *
 * Gives reports on the different types of transactions.
 *
 * @package App
 * @author Deric Cain
 */
class Report
{

    /**
     * Report constructor.
     */
    public function __construct()
    {
//        if (env('APP_ENV') == 'production') {
        \Stripe\Stripe::setApiKey(env('STRIPE_LIVE_SECRET'));
//        } else {
//            \Stripe\Stripe::setApiKey(env('STRIPE_TEST_SECRET'));
//        }
    }


    /**
     * Get all of the transactions for last month.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getLastMonth()
    {
        new static();

        return collect(\Stripe\Charge::all([
            'limit' => 100,
            'created' => [
                'gte' => Carbon::today()->subMonth()->startOfMonth()->timestamp,
                'lte' => Carbon::today()->startOfMonth()->subDay()->timestamp,
            ]
        ])['data']);
    }

    /**
     * Get this months transactions.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getThisMonth()
    {
        new static();

        return collect(\Stripe\Charge::all([
            'limit' => 100,
            'created' => [
                'gte' => Carbon::today()->startOfMonth()->timestamp,
                'lte' => Carbon::today()->timestamp,
            ]
        ])['data']);
    }

    /**
     * Get the grand total for the previous month.
     *
     * @return int
     */
    public static function previousMonthTotal()
    {
        return self::getLastMonth()->reduce(function ($previous, $current) {
            return $previous + ($current['amount'] - $current['refunded']);
        });
    }

    /**
     * Get the grand total for the current month.
     *
     * @return int
     */
    public static function currentMonthTotal()
    {
        return self::getThisMonth()->reduce(function ($previous, $current) {
            return $previous + ($current['amount'] - $current['refunded']);
        });
    }

    /**
     * Get either the current or previous month's totals based on the
     * type of transaction.
     *
     * @param $transactionType
     * @param bool $previous
     * @return mixed
     */
    public static function filterByTypeAndDate($transactionType, $previous = false)
    {
        if ($previous) {
            return self::getPreviousMonthOfType($transactionType);
        }

        return self::getCurrentMonthOfType($transactionType);
    }

    /**
     * Get the previous month's intake totals.
     *
     * @return mixed
     */
    public static function previousIntakes()
    {
        return self::filterByTypeAndDate('intake', true);
    }

    /**
     * Get the current month's intake totals.
     *
     * @return mixed
     */
    public static function currentIntakes()
    {
        return self::filterByTypeAndDate('intake');
    }

    /**
     * Get the previous month's tuition totals.
     *
     * @return mixed
     */
    public static function previousTuition()
    {
        return self::filterByTypeAndDate('tuition', true);
    }

    /**
     * Get the current month's tuition totals.
     *
     * @return mixed
     */
    public static function currentTuition()
    {
        return self::filterByTypeAndDate('tuition');
    }

    /**
     * Get current month's transactions based on the type of transaction.
     *
     * @param $transactionType
     * @return mixed
     */
    private static function getCurrentMonthOfType($transactionType)
    {
        return self::getThisMonth()->filter(function ($transaction) use ($transactionType) {
            return $transaction['description'] == $transactionType;
        })->reduce(function ($previous, $current) {
            return $previous + ($current['amount'] - $current['amount_refunded']);
        });
    }

    /**
     * Get previous month's transactions based on the type of transaction.
     *
     * @param $transactionType
     * @return mixed
     */
    private static function getPreviousMonthOfType($transactionType)
    {
        return self::getLastMonth()->filter(function ($transaction) use ($transactionType) {
            return $transaction['description'] == $transactionType;
        })->reduce(function ($previous, $current) {
            return $previous + ($current['amount'] - $current['amount_refunded']);
        });
    }

    /**
     * Build array for JS.
     *
     *
     *
     * @return array
     */
    public static function buildChartData()
    {
        new static();

        $endDate = Carbon::today()->timestamp;
        $startDate = Carbon::today()->firstOfMonth()->timestamp;

        $keepGoing = true;
        $counter = 0;
        $reportArray = [];
        $reportArray['categories'] = [];
        $reportArray['type'] = [];
        $reportArray['type']['total'] = [];
        $reportArray['type']['intake'] = [];
        $reportArray['type']['tuition'] = [];


        while ($keepGoing) {
            $stripeParameters = [ 'limit' => 100 ];
            if ($counter === 0) {
                $stripeParameters['created'] = [
                    'gte' => Carbon::today()->firstOfMonth()->timestamp,
                    'lte' => Carbon::today()->timestamp,
                ];
            } else {
                $stripeParameters['created'] = [
                    'gte' => Carbon::createFromTimestamp($startDate)->subMonths($counter)->firstOfMonth()->timestamp,
                    'lte' => Carbon::createFromTimestamp($endDate)->subMonths($counter)->endOfMonth()->timestamp,
                ];
            }
            $transactions = collect(\Stripe\Charge::all($stripeParameters)['data']);

            $sum = $transactions->sum(function ($current) {
                return $current['amount'] - $current['refunded_amount'];
            });

            $intakeSum = $transactions->filter(function ($transaction) {
                return $transaction['description'] == 'intake';
            })->sum(function ($current) {
                return $current['amount'] - $current['refunded_amount'];
            });

            $tuitionSum = $transactions->filter(function ($transaction) {
                return $transaction['description'] == 'tuition';
            })->sum(function ($current) {
                return $current['amount'] - $current['refunded_amount'];
            });

            if ($sum > 0 || $counter === 0) {
                $reportArray['categories'][$counter] = Carbon::createFromTimestamp($startDate)->subMonths($counter)
                    ->firstOfMonth()->format('M Y');
                array_push($reportArray['type']['total'], ($sum / 100));
                array_push($reportArray['type']['intake'], ($intakeSum / 100));
                array_push($reportArray['type']['tuition'], ($tuitionSum / 100));

            } else {
                $keepGoing = false;
            }
            $counter++;
        }

        $reportCollection = collect($reportArray)->toJson();

        return $reportCollection;
    }
}