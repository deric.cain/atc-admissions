<?php
namespace App;

use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Customer as StripeCustomer;

class Customer
{

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        if (env('APP_ENV') == 'production') {
            Stripe::setApiKey(env('STRIPE_LIVE_SECRET'));
        } else {
            Stripe::setApiKey(env('STRIPE_TEST_SECRET'));
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function all()
    {
        new static();

        return collect(StripeCustomer::all());
    }

    /**
     * Charge the customer a one-time, ad-hoc fee.
     *
     * @param $request
     */
    public static function charge($request)
    {
        new static();
        $client = Client::findByCustomerId($request['customer_id']);
        try {
            Charge::create([
                'amount' => self::convertFloatToInt($request['amount']),
                'currency' => 'usd',
                'description' => $request['type'],
                'customer' => $request['customer_id'],
                'metadata' => [
                    'student_name' => $client->student_name,
                    'payee_name' => $client->payee_name,
                    'payee_email' => $client->payee_email,
                    'entry_date' => $client->entry_date,
                    'program' => $client->program,
                    'address' => $client->address,
                    'city' => $client->city,
                    'state' => $client->state,
                    'zip' => $client->zip,
                    'phone' => $client->phone
                ],
                'receipt_email' => $client->clientpayee_email
            ]);
        } catch (\Stripe\Error\Card $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\InvalidRequest $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\Authentication $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\ApiConnection $e) {
            echo json_encode($e->getJsonBody());
            die;
        } catch (\Stripe\Error\Base $e) {
            echo json_encode($e->getJsonBody());
            die;
        }
    }

    /**
     * Find a Stripe customer by their ID.
     *
     * @param $customerId
     * @return StripeCustomer
     */
    public static function find($customerId)
    {
        new static();
        return StripeCustomer::retrieve($customerId);
    }


    /**
     * Create a customer in Stripe.
     *
     * @param $customerData
     * @return StripeCustomer
     */
    public static function create($customerData)
    {
        new static();
        return StripeCustomer::create($customerData);
    }

    /**
     * Stripe uses int instead of floats so we have to convert them.
     *
     * @param $number
     * @return mixed
     */
    private static function convertFloatToInt($number)
    {
        return explode('.', $number)[0] . '00';
    }
}