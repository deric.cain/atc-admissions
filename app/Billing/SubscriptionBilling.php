<?php

namespace App\Billing;

use Carbon\Carbon;
use Stripe\Charge;
use Stripe\Stripe;
use App\Subscription;

class SubscriptionBilling
{
    /**
     * The request we will use to process payment.
     *
     * @var Subscription
     */
    private $subscription;

    /**
     * Subscription constructor.
     *
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        if (env('APP_ENV') == 'production') {
            Stripe::setApiKey(env('STRIPE_LIVE_SECRET'));
        } else {
            Stripe::setApiKey(env('STRIPE_TEST_SECRET'));
        }
        $this->subscription = $subscription;
    }

    /**
     * Charge the customer for services rendered.
     *
     * @return mixed
     */
    public function charge()
    {
        Charge::create([
            'amount' => $this->subscription->amount,
            'currency' => 'usd',
            'description' => $this->subscription->type,
            'customer' => $this->subscription->client->customer_id,
            'metadata' => [
                'student_name' => $this->subscription->client->student_name,
                'payee_name' => $this->subscription->client->payee_name,
                'payee_email' => $this->subscription->client->payee_email,
                'entry_date' => Carbon::parse($this->subscription->client->entry_date)->toDateString(),
                'program' => $this->subscription->client->program,
                'address' => $this->subscription->client->address,
                'city' => $this->subscription->client->city,
                'state' => $this->subscription->client->state,
                'zip' => $this->subscription->client->zip,
                'phone' => $this->subscription->client->phone
            ],
            'receipt_email' => $this->subscription->client->payee_email
        ]);

        $this->decrementSubscription();
    }

    /**
     * Decrement the amount of payments left on the current subscription.
     */
    private function decrementSubscription()
    {
        if($this->isLastPayment()) {
            $this->endSubscription();
        } else {
            $this->decrementMonth();
        }
    }

    /**
     * See if we have any payments left after this one.
     *
     * @return bool
     */
    private function isLastPayment()
    {
        return $this->subscription->number_of_payments_left == 1;
    }

    /**
     * End a subscription.
     */
    private function endSubscription()
    {
        $this->subscription->number_of_payments_left = 0;
        $this->subscription->next_payment = null;
        $this->subscription->save();
    }

    /**
     * Subtract a month from the subscription.
     */
    private function decrementMonth()
    {
        $this->subscription->number_of_payments_left -= 1;
        $this->subscription->next_payment = Carbon::parse($this->subscription->next_payment)->addMonth()
            ->toDateString();
        $this->subscription->save();
    }
}