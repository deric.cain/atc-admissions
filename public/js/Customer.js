(function () {

    var $modalTitle = $('#payment-modal-title');
    var $makePaymentBtn = $('.charge-customer');
    var $paymentWarning = $('.payment-warning');
    var $customerIdInput = $('input[name=customer_id]');
    var $formChargeCustomer = $('#form-charge-customer');
    var $spinner = $('.spinner');
    var $btnSubmit = $('#customer-charge-btn');
    var $paymentModal = $('#payment-modal');
    var $customerTable  = $('#table-customers');
    var $customerPhoneNumber = $('.customer-phone-number');

    var _ = {

        /**
         * Methods we want to run on page loaded.
         */
        init: function() {
            this.addCustomerNameToTitle();
            this.handleFormSubmission();
            this.buildTable();
            this.formatPhoneNumber();
        },

        buildTable: function() {
            $customerTable.DataTable({
                'pageLength': 50,
                'order': [[0, 'asc']],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": false }
                ]
            });
        },

        formatPhoneNumber: function() {
            $customerPhoneNumber.each(function() {
                $(this).text(_.addSeparatorToPhoneNumber($(this).text()));
            });
        },

        addSeparatorToPhoneNumber: function(number) {
            return number.replace(/(\d{3})(\d{3})(\d{4})/, '$1.$2.$3');
        },

        /**
         * Show the progress spinner.
         */
        showSubmitProcessing: function () {
            $spinner.show();
            $btnSubmit.prop('disabled', true);
            $btnSubmit.addClass('btn-success').removeClass('btn-primary').html('<img class="spinner" height="30px" width="30px" src="images/spinner.svg">Processing...');
        },

        /**
         * Hide the progress spinner. If the submission is completed, then display completed.
         *
         * @param completed
         */
        hideSubmitProcessing: function (completed) {
            $spinner.hide();
            if (completed) {
                $btnSubmit.html('Completed');
            } else {
                $btnSubmit.prop('disabled', false);
                $btnSubmit.addClass('btn-primary').removeClass('btn-success').html('Submit');
            }
        },

        /**
         * Make the modal title the customer's name so the user knows who they are charging.
         */
        addCustomerNameToTitle: function() {
            $makePaymentBtn.on('click', function() {
                $modalTitle.text('Payment for ' + $(this).data('customer-name'));
                $paymentWarning.text('You are about to charge a payment to ' + $(this).data('customer-name') + '.');
                $customerIdInput.val($(this).data('id'));
            });
        },

        /**
         * Handle the charge method on the Customer class using AJAX
         */
        handleFormSubmission: function() {
            $formChargeCustomer.submit(function(event) {
                event.preventDefault();
                _.showSubmitProcessing();
                var data = $formChargeCustomer.serialize();
                var url = this.action;
                $.ajax({
                    url: url,
                    data: data,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(response) {
                        $paymentModal.modal('hide');
                        swal({
                            title: 'Payment posted!',
                            text: 'The payment has been posted!',
                            timer: 3000,
                            type: 'success'
                        }, function() {
                            _.hideSubmitProcessing(true);
                        });
                        setTimeout(function() {
                            window.location = '/transactions';
                        }, 3000);
                    },
                    error: function(response) {
                        console.log(response);
                    }
                })
            });
        }
    };

    /**
     * Initialize our scripts/
     */
    _.init();

})();
//# sourceMappingURL=Customer.js.map
