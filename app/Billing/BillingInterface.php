<?php

namespace App\Billing;

/**
 * Interface BillingInterface
 * 
 * @package App\Billing
 */
interface BillingInterface
{
    /**
     * Handle the payment process.
     *
     * @return mixed
     */
    public function execute();

    /**
     * Create the client in our database.
     *
     * @return mixed
     */
    public function createClientInDb();

    /**
     * Charge the customer for services rendered.
     *
     * @param $type
     * @param $amount
     * @return mixed
     */
    public function charge($type, $amount);


    /**
     * Create a subscription.
     *
     * @param $type
     * @param $nummberOfPayments
     * @return mixed
     */
    public function createSubscription($type, $nummberOfPayments);
}