<?php

use App\Customer;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomerTest extends TestCase
{
    /**
     * Our test user.
     * @var App\User
     */
    public $user;

    function setUp()
    {
        parent::setUp();
        $this->user = factory(App\User::class)->create();
    }

    /** @test */
    function user_sees_a_customer_table_when_visiting_customers_page()
    {
        $this->actingAs($this->user)
            ->visit('/customers')
            ->see('Customers');
    }

    /** @test */
    function a_user_must_be_logged_in_to_view_customers_page()
    {
        $this->visit('/customers')
            ->dontSee('Customers')
            ->see('Login');
    }

    /** @test */
    function a_call_to_get_all_customers_should_return_an_array()
    {
        $customers = Customer::all();
        $this->assertInstanceOf('Illuminate\Support\Collection', $customers);
    }

    /** @test */
    function a_single_customer_payment_should_post()
    {
        $this->actingAs($this->user)
            ->visit('/customers')
            ->press('Make Payment')
            ->type('100', 'amount')
            ->press('Post Payment')
            ->seePageIs('/customers');
    }
}
