@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Reports</h1>
            </div>
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table id="reports-table" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Last Month</th>
                                <th>This Month</th>
                                <th>(+/-)Change %</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Grand Total</td>
                            <td>${{ number_format(($previousTotal / 100), 2) }}</td>
                            <td>${{ number_format(($currentTotal / 100), 2) }}</td>
                            @if((($currentTotal - $previousTotal) / $previousTotal) < 0)
                                <td class="text-danger">{{ number_format((($currentTotal - $previousTotal) /
                                $previousTotal) *
                                 100, 1)
                                }}%</td>
                            @else
                                <td class="text-success">+{{ number_format((($currentTotal - $previousTotal) /
                                $previousTotal) *
                                100, 1)
                                }}%</td>
                            @endif
                        </tr>
                        <tr>
                            <td>Intake Total</td>
                            <td>${{ number_format(($previousIntake / 100), 2) }}</td>
                            <td>${{ number_format(($currentIntake / 100), 2) }}</td>
                            @if((($currentIntake - $previousIntake) / $previousIntake) < 0)
                                <td class="text-danger">{{ number_format((($currentIntake - $previousIntake) /
                                $previousIntake) * 100, 1)
                                 }}%</td>
                            @else
                                <td class="text-success">+{{ number_format((($currentIntake - $previousIntake) /
                                $previousIntake) * 100, 1)
                                 }}%</td>
                            @endif
                        </tr>
                        <tr>
                            <td>Tuition Total</td>
                            <td>${{ number_format(($previousTuition / 100), 2) }}</td>
                            <td>${{ number_format(($currentTuition / 100), 2) }}</td>
                            @if((($currentTuition - $previousTuition) / $previousTuition) < 0)
                                <td class="text-danger">{{ number_format((($currentTuition - $previousTuition) /
                                $previousTuition) * 100,
                                 1) }}%</td>
                            @else
                                <td class="text-success">+{{ number_format((($currentTuition - $previousTuition) /
                                $previousTuition) * 100,
                                 1) }}%</td>
                            @endif
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-12 chart"></div>
        </div>
    </div>

@endsection

@section('css')

@endsection

@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="{{ elixir('js/Chart.js') }}"></script>
@endsection

