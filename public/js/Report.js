(function() {

    var $tableTransactions = $('#table-transactions');
    var $time = $('.time');
    var $entryDate = $('.entry-date');

    var _ = {

        /**
         * Wait until the DOM is fully rendered before firing these events.
         */
        windowLoad: function() {
            $(window).load(function() {
                $time.each(function () {
                    $(this).text(moment.unix(parseInt($(this).text())).format('MM/DD/Y h:mm a'));
                });
                _.convertEntryDate();
            });
        },


        /**
         * Use DataTables to format the table.
         */
        loadTable: function() {
            $tableTransactions.DataTable({
                'pageLength': 50,
                'order': [[0, 'desc']],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": true },
                ]
            });
        },

        /**
         * Use Moment.js to convert Stripe's time format, which is
         * Unix.
         */
        convertTime: function() {
            moment($time).format();
        },

        /**
         * Again, use Moment to convert the entry date to a readable format.
         */
        convertEntryDate: function() {
            $entryDate.each(function() {
                $(this).text(moment($(this).text()).format('MM/DD/Y'));
            });
        },

        /**
         * Initialize all of our methods here.
         */
        init: function () {
            this.loadTable();
            this.windowLoad()
        }
    };

    _.init();

})();

//# sourceMappingURL=Report.js.map
