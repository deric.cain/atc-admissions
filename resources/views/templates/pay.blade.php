@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            {!! BootForm::open()->id('form-payment')->action('/') !!}

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>General Info</h4>
                        {!! BootForm::select('Which program?', 'program')->options([
                            '' => '',
                            'Bay Minette' => 'Bay Minette',
                            'Hayden' => 'Hayden',
                            'Lincoln' => 'Lincoln',
                            'Selma' => 'Selma',
                        ]) !!}
                        {!! BootForm::text('Student\'s Name', 'student_name')->required() !!}
                    </div>
                    <div class="col-sm-6">
                        {!! BootForm::text('Payee\'s Name', 'payee_name')->required() !!}
                    </div>
                    <div class="col-sm-6">
                        {!! BootForm::email('Payee\'s Email', 'payee_email')
                        ->required()
                         !!}
                    </div>
                    <div class="col-sm-12">
                        {!! BootForm::text('Address', 'address')->required() !!}
                    </div>
                    <div class="col-sm-5">
                        {!! BootForm::text('City', 'city')->required() !!}
                    </div>
                    <div class="col-sm-3">
                        {!! BootForm::select('State', 'state')->options($states) !!}
                    </div>
                    <div class="col-sm-4">
                        {!! BootForm::text('Zip', 'zip')->required() !!}
                    </div>
                </div>
                {!! BootForm::text('Phone (Use 10 digits)', 'phone')->class('form-control') !!}
                {!! BootForm::text('Entry Date', 'entry_date')->class('form-control datepicker')
                ->required() !!}
            </div>
            <div class="col-sm-4">
                <h4>Payment Info</h4>
                {!! BootForm::text('Intake Amount (How much will they pay today?)', 'intake_amount') !!}
                {!! BootForm::checkbox('Some of the intake fees will be paid in installments.', 'installments') !!}
                <div class="installments">
                    {!! BootForm::text('How much will they pay in installments?', 'installment_intake_amount') !!}
                    {!! Bootform::select('How many months will it take to pay the induction fees?','installment_months')->options([
                        '1' => 1,
                        '2' => 2,
                        '3' => 3,
                        '4' => 4,
                        '5' => 5,
                        '6' => 6,
                    ]) !!}
                </div>
                <hr>
                {!! BootForm::text('Tuition Amount', 'tuition_amount') !!}
                {!! BootForm::checkbox('Make tuition recurring?', 'tuition_recurring') !!}
                <div id="tuition-daterange" class="row">
                    <div class="col-sm-6">
                        {!! BootForm::text('Tuition Start Date', 'tuition_start_date')->class('form-control
                        tuition-datepicker') !!}
                    </div>
                    <div class="col-sm-6">
                        {!! BootForm::text('Tuition End Date', 'tuition_end_date')->class('form-control tuition-datepicker') !!}
                    </div>
                </div>
                <hr>
                {!! BootForm::text('Bus Fee Amount', 'bus_fee_amount') !!}
            </div>
            <div class="col-sm-4">
                <h4>Credit Card Info</h4>
                <div class="form-group">
                    <label class="control-label" for="cc_number">Card Number</label>
                    <input type="text" id="cc_number" class="form-control" required>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Exp. Month</label>
                                        <select class="form-control" id="cc_month">
                                        @for ($i = 1; $i < 13; $i++)
                                           <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Exp. Year</label>
                                        <select class="form-control" id="cc_year">
                                            @for ($i = date('Y'); $i < date('Y') + 10; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="cvc">CVC</label>
                                <input type="text" id="cvc" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row payment-errors">
                    <div class="col-sm-12">
                        <div class="alert alert-danger errors">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <button class="btn btn-block btn-primary" type="submit" id="btn-submit"><img class="spinner"
                            src="{{ url('images/spinner.svg') }}"> Submit</button>
            </div>
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.4.1/jquery.payment.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="{{ elixir('js/Billing.js') }}"></script>
@endsection