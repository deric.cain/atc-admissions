<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    /**
     * Define mass assignments.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'next_payment',
        'number_of_payments_left',
        'amount',
        'type',
        'tuition_start_date',
        'tuition_end_date'
    ];

    /**
     * Create mutator for dates.
     *
     * @var array
     */
    protected $dates = [
        'created_on',
        'next_payment',
        'updated_on',
        'tuition_start_date',
        'tuition_end_date'
    ];


    /**
     * Subscriptions belong to Clients.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get all of the subscriptions that need to be charged today.
     *
     * @return Collection
     */
    public function scopeChargeToday($query)
    {
        return $query->where('next_payment', Carbon::today('America/Chicago')->toDateString())
            ->get();
    }

    /**
     * Get all of the active subscriptions.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('number_of_payments_left', '>', 0)
            ->orderBy('created_at', 'desc')
            ->with('client')
            ->get();
    }

    /**
     * Get all of the inactive subscriptions.
     *
     * @param $query
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where('number_of_payments_left', '=', 0)
            ->with('client')
            ->get();
    }
}
