<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientTest extends TestCase
{
    /** @test */
    function a_new_client_is_created()
    {
        $user = factory(User::class)->create();
        $this->assertTrue($user instanceof User);
    }

}
