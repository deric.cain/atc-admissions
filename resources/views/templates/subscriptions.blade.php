@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Active Subscriptions</h2>
                <div class="table-responsive">
                    <table id="table-subscriptions" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Created on</th>
                            <th>Contact's Name</th>
                            <th>Next payment on</th>
                            <th># payments remaining</th>
                            <th>Amount</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($active as $subscription)
                            <tr>
                                <td>{{ $subscription->created_at->format('m/d/y') }}</td>
                                <td>{{ $subscription->client->payee_name }}</td>
                                <td>{{ $subscription->next_payment->format('m/d/y') }}</td>
                                <td>{{ $subscription->number_of_payments_left }}</td>
                                <td>${{ number_format(($subscription->amount / 100), 2) }}</td>
                                <td class="text-right"><button class="btn btn-danger btn-xs cancel-subscription"
                                                               data-id="{{ $subscription->id }}">Cancel
                                        Subscription</button></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-12">
                <hr>
            </div>
            <div class="col-sm-12">
                <h2 class="text-muted">In-active Subscriptions</h2>
                <div class="table-responsive">
                    <table id="table-inactive-subscriptions" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Created on</th>
                            <th>Contact's Name</th>
                            <th>Cancelled on</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($inactive as $subscription)
                            <tr>
                                <td>{{ $subscription->created_at->format('m/d/y') }}</td>
                                <td>{{ $subscription->client->payee_name }}</td>
                                <td>{{ $subscription->updated_at->format('m/d/y') }}</td>
                                <td>${{ number_format(($subscription->amount / 100), 2) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.css" type="text/css">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.js"></script>
<script src="{{ elixir('js/Subscription.js') }}"></script>
@endsection
