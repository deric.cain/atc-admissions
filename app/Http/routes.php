<?php

use App\Report;


Route::auth();
Route::get('/', 'PaymentController@index');
Route::post('/', [
    'uses' => 'PaymentController@postPayment',
    'as' => 'create_payment'
]);
Route::get('/cancel/{id}', [
    'uses' => 'PaymentController@cancelSubscription',
]);
Route::get('/transactions', 'ReportController@transactions');
Route::get('/recurring', 'ReportController@subscriptions');
Route::get('/reports', 'ReportController@reports');
Route::get('/chart', 'ReportController@chart');

Route::get('/customers', 'CustomerController@index');
Route::post('/customers', 'CustomerController@chargeCustomer');

// We don't want users to be able to sign up
Route::any('/register', function() {
    return redirect('/');
});

