<style>
    * {
        font-size: 15px;
    }
</style>
<table cellspacing="0" cellpadding="10" border="1">
    <thead>
    <tr>
        <th style="background-color:#eee" width="80">Amount</th>
        <th style="background-color:#eee" width="100">Type</th>
        <th style="background-color:#eee" width="200">Payer's Name</th>
        <th style="background-color:#eee" width="200">Student's Name</th>
    </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        <tr>
            <td width="80">{{ $transaction->amount }}</td>
            <td width="100">{{ ucfirst($transaction->type) }}</td>
            <td width="200">{{ $transaction->client->payee_name }}</td>
            <td width="200">{{ $transaction->client->student_name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>